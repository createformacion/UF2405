﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UF2405.Models
{
    public class Maziza
    {

        public int id { get; set; }
        public string name { get; set; }
        public bool silicona { get; set; }
        public int edad { get; set; }
        public string imagen { get; set; }

    }
}
