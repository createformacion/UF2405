﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UF2405.Models
{
    public class MazizaContext : DbContext
    {
        public DbSet<Maziza> maciza { get; set; }


        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            string sConnection = "Server=(LocalDb)\\MSSQLLocalDB;Database=MacizaDB;";
            optionsBuilder.UseSqlServer(sConnection);
        }
    }
}
