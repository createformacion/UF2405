﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using UF2405.Models;

namespace UF2405.Migrations
{
    [DbContext(typeof(MazizaContext))]
    partial class MazizaContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.0.1")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("UF2405.Models.Maziza", b =>
                {
                    b.Property<int>("id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("edad");

                    b.Property<string>("imagen");

                    b.Property<string>("name");

                    b.Property<bool>("silicona");

                    b.HasKey("id");

                    b.ToTable("maciza");
                });
        }
    }
}
