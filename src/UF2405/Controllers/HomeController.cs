﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using UF2405.Models;

namespace UF2405.Controllers
{
    public class HomeController : Controller
    {
        private MazizaContext db;

        public HomeController(MazizaContext context)
        {
            db = context;
        }
        
        public IActionResult Index()
        {
            db.maciza.ToList();
            return View();
        }

        /*   public IActionResult Create()
           {
               return View();
           }
           */

        [HttpPost]
        public IActionResult Create([Bind(include: "id,name,silicona,edad")] Maziza maz)
        {
            if (ModelState.IsValid)
            {
                db.maciza.Add(maz);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(maz);
        }

    }
}
