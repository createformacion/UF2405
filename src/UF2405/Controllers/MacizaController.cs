﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using UF2405.Models;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace UF2405.Controllers
{
    public class MacizaController : Controller
    {
        private MazizaContext db;

        public MacizaController(MazizaContext context)
        {
            db = context;
        }

        public IActionResult Index()
        {
            db.maciza.ToList();
            return View();
        }
    }
}
